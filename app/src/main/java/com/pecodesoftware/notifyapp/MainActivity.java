package com.pecodesoftware.notifyapp;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    public static final int FIRST_FRAGMENT = 1;
    public static final int OTHER_FRAGMENT = 0;

    private Integer number;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setPagesComponents();
        number = getIntent().getIntExtra("number", 0);
        addFirstFragment();
    }

    public void addFragment(Integer isFirstFragment) {
        addFragmentByNumber(isFirstFragment);
    }

    private void addFirstFragment() {
        if (number == 0)
            addFragment(FIRST_FRAGMENT);
        else{
            number--;
            addFragmentByNumber(FIRST_FRAGMENT);}
    }

    private void addFragmentByNumber(Integer isFirstFragment) {
        number++;
        Bundle bundle = new Bundle();
        bundle.putInt("number", number);
        bundle.putInt("isFirstFragment", isFirstFragment);
        NotifyFragment notifyFragment = new NotifyFragment();
        notifyFragment.setArguments(bundle);
        viewPagerAdapter.addFragment(notifyFragment);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(viewPagerAdapter.getCount() - 1);
    }

    public void removeFragment(NotifyFragment fragment) {
        fragment.removeAllNotifications();
        viewPagerAdapter.removeFragment(fragment);
        viewPagerAdapter.notifyDataSetChanged();
        viewPager.setCurrentItem(viewPagerAdapter.getCount() - 1);
    }

    private void setPagesComponents() {
        viewPager = findViewById(R.id.viewpager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
    }
}
