package com.pecodesoftware.notifyapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class NotifyFragment extends Fragment {

    private static Integer notificationId = 0;

    private TextView tvNumber;
    private TextView tvCreateNotification;
    private ImageView imMinus;
    private ImageView imPlus;
    private FrameLayout frameMinus;
    private NotificationManager notificationManager;

    private Integer number;
    private Integer isFirstFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notify, container, false);

        initFields(view);
        setListeners();

        number = getArguments().getInt("number");
        tvNumber.setText(number.toString());

        isFirstFragment = getArguments().getInt("isFirstFragment", MainActivity.OTHER_FRAGMENT);

        checkIsFirstFragment();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void setListeners() {
        imPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).addFragment(MainActivity.OTHER_FRAGMENT);
            }
        });

        imMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).removeFragment(NotifyFragment.this);
            }
        });

        tvCreateNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNotification();
            }
        });
    }

    private void checkIsFirstFragment() {
        if (isFirstFragment == MainActivity.FIRST_FRAGMENT) {
            frameMinus.setVisibility(View.GONE);
        }
    }

    private void initFields(View view) {
        notificationManager = (NotificationManager) getActivity()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        tvCreateNotification = view.findViewById(R.id.btn_create_notify);
        tvNumber = view.findViewById(R.id.tv_number);
        imMinus = view.findViewById(R.id.btn_minus);
        imPlus = view.findViewById(R.id.btn_plus);
        frameMinus = view.findViewById(R.id.frame_minus);
    }

    private void showNotification() {
        Intent resultIntent = new Intent(getContext(), MainActivity.class);
        resultIntent.putExtra("number", number);
        PendingIntent resultPendingIntent = PendingIntent
                .getActivity(getContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification =
                new NotificationCompat.Builder(getContext())
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("You create a notification")
                        .setContentText("Notification " + number)
                        .setContentIntent(resultPendingIntent)
                        .setAutoCancel(true)
                        .build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel =
                    new NotificationChannel(null, null, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(mChannel);
        }

        notificationManager.notify(notificationId, notification);
        notificationId++;
    }

    public void removeAllNotifications() {
        if (notificationManager != null)
            notificationManager.cancelAll();
    }
}
