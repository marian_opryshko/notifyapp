package com.pecodesoftware.notifyapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private FragmentManager fragmentManager;
    private List<Fragment> fragments;

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
        fragmentManager=manager;
        fragments = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    public void addFragment(Fragment fragment) {
        fragments.add(fragment);
    }

    public void removeFragment(Fragment fragment) {
        fragments.remove(fragment);
        fragmentManager.beginTransaction().remove(fragment).commit();

    }

}